import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om

def ZivaBuildTissue():
  attrbuteobject = mc.ls(sl= True)[0]
  tetSize = 6.0
  youngsModulus = 4.0
  surfaceRefinement = 1.0
  contactStiffnessExp = 6
  mc.select(attrbuteobject)
  mm.eval('ziva -t;')
  
  zTetNode = mm.eval('zQuery -t zTet;')
  zTetRename = attrbuteobject + "_zTet"
  mc.rename(zTetNode, zTetRename)
  mc.setAttr(zTetRename + ".tetSize", tetSize)
  mc.setAttr(zTetRename + ".surfaceRefinement", surfaceRefinement)
  
  zTissueNode = mm.eval('zQuery -t zTissue;')
  zTissueRename = attrbuteobject + "_zTissue"
  mc.rename(zTissueNode, zTissueRename)
  mc.setAttr(zTissueRename + ".contactStiffnessExp", contactStiffnessExp)

  zGeoNode = mm.eval('zQuery -t zGeo;')
  zGeoNodeRename = attrbuteobject + "_zGeo"
  mc.rename(zGeoNode, zGeoNodeRename)

  zMaterialNode = mm.eval('zQuery -t zMaterial;')
  zMaterialRename = attrbuteobject + "_zMaterial"
  mc.rename(zMaterialNode, zMaterialRename)
  mc.setAttr(zMaterialRename + ".youngsModulus", youngsModulus)

def ZivaBuildBone():
  attrbuteBoneobject = mc.ls(sl= True)[0]
  contactStiffnessExp = 5
  mc.select(attrbuteBoneobject)
  mm.eval('ziva -b;')
  
  zBoneNode = mm.eval('zQuery -t zBone;')
  zBoneRename = attrbuteBoneobject + "_zBone"
  mc.rename(zBoneNode, zBoneRename)
  mc.setAttr(zBoneRename + ".contactStiffnessExp", contactStiffnessExp)
  zGeoNode = mm.eval('zQuery -t zGeo;')
  zGeoNodeName = attrbuteBoneobject + "_zGeo"
  mc.rename(zGeoNode, zGeoNodeName)
  
  
def ZivaBuildFiber():
  srcObject = mc.ls(sl= True)[0]
  strength = 18.4
  strengthExp = 3
  mc.select(srcObject)
  mm.eval('ziva -f;')
  zFiberNode = mm.eval('zQuery -t zFiber;')
  zFiberRename = srcObject + "_zFiber"
  mc.rename(zFiberNode, zFiberRename)
  mc.setAttr(zFiberRename + ".strength", strength)
  mc.setAttr(zFiberRename + ".strengthExp", strengthExp)
  
def ZivaBuildMaterial():
  srcObject = mc.ls(sl= True)[0]
  mc.select(srcObject)
  mm.eval('ziva -m;')
  zMaterialNode = mm.eval('zQuery -t zMaterial;')
  print zMaterialNode
  zMaterialRename = srcObject + "_Created_zMaterial_0"
  mc.rename(zMaterialNode[-1], zMaterialRename)

def ZiveMakeAttachements():
    attachmentMode = 1
    stiffness = 3.0
    stiffnessExp = 6
    attrbuteobject = mc.ls(sl= True)
    sourceObject = attrbuteobject[-1]
    for attachmentObject in attrbuteobject[:-1]:
        mc.select(sourceObject, attachmentObject, r= True)
        attachmentNode = mm.eval('ziva -a;')
        attachmentRename = sourceObject + "_" + attachmentObject
        mc.rename(attachmentNode, attachmentRename)
        mc.setAttr(attachmentRename + ".attachmentMode", attachmentMode)
        mc.setAttr(attachmentRename + ".stiffness", stiffness)
        mc.setAttr(attachmentRename + ".stiffnessExp", stiffnessExp)
        