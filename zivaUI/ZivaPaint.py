from pymel.core import *
import os
from loadUiFile import get_maya_window, load_ui_type
import loadUiFile

import maya.mel as mm
import maya.cmds as mc
import zBuilder.zMaya as zMaya
import zBuilder.setup.Ziva as zva

ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Ziva2.ui')
list_form, list_base = load_ui_type(ui_file)

class ZivaPaintUI(list_form, list_base):
	def __init__(self, parent = get_maya_window()):
		self.window_name = 'ZivaPaintScript'
		if window(self.window_name, exists = True):
			deleteUI(self.window_name)
			
		super(ZivaPaintUI, self).__init__(parent)
		self.paintWidgetSel = ""
		self.setupUi(self)
		self.generateMuscleList()
		self.generateZNodeList()
		self.MuscleQComboBox.currentIndexChanged.connect(self.populatePaintLayer)
		self.ZNodeQComboBox.currentIndexChanged.connect(self.populatePaintLayer)
		self.Paint_QListWidget.itemPressed.connect(self.populateSourceConnection)
		self.Paint_QButton.clicked.connect(self.paintCommand)
	def printTest(self):
		print str(self.MuscleQComboBox.currentText())
	def generateMuscleList(self):
		mc.select(cl=True)
		musclesInScene = mm.eval('zQuery -t "zTissue" -m')
		self.muscleList = []
		self.MuscleQComboBox.clear()
		for muscles in musclesInScene:
			if cmds.objExists(muscles):
				self.MuscleQComboBox.addItem(muscles)
				self.muscleList.append(muscles)
	
	def populateSourceConnection(self):
		self.Source_QListWidget.clear()
		self.paintWidgetSel = str(self.Paint_QListWidget.currentItem().text())
		extractMeshData = zMaya.get_association(self.paintWidgetSel)
		for mesh in extractMeshData:
			self.Source_QListWidget.addItem(mesh)
	
	def generateZNodeList(self):
		self.ZNodeList = ["zAttachments", "zBones", "zMaterials", "zFibers", "zTets"]
		for ZNode in self.ZNodeList:
			self.ZNodeQComboBox.addItem(ZNode)

			
	def populatePaintLayer(self):
		self.Paint_QListWidget.clear()
		self.muscle = str(self.MuscleQComboBox.currentText())
		self.ZNodetype = str(self.ZNodeQComboBox.currentText())
		if self.ZNodetype == "zAttachments":
			appendPaintList = zMaya.get_zAttachments(self.muscle)
		if self.ZNodetype == "zBones":
			appendPaintList = zMaya.get_zBones(self.muscle)
		if self.ZNodetype == "zMaterials":
			appendPaintList = zMaya.get_zMaterials(self.muscle)
		if self.ZNodetype == "zFibers":
			appendPaintList = zMaya.get_zFibers(self.muscle)
		if self.ZNodetype == "zTets":
			appendPaintList = zMaya.get_zTets(self.muscle)
		for x in appendPaintList:
			self.Paint_QListWidget.addItem(x)
			
	def paintCommand(self):
		sourceSelectionList = [str(sourceObject.text()) for sourceObject in self.Source_QListWidget.selectedItems()]		
		mc.select(sourceSelectionList)
		mm.eval( 'artSetToolAndSelectAttr( "artAttrCtx", "' + self.ZNodetype + "." + self.paintWidgetSel + '.weights")')


def run_plugin():
	ex = ZivaPaintUI()
	ex.show()
