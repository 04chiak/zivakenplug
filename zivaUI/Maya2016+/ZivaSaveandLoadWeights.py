from pymel.core import *
import os
from loadUiFile import get_maya_window, load_ui_type
import loadUiFile
import maya.mel as mm
import maya.cmds as mc
import zBuilder.zMaya as zMaya
import zBuilder.builders.ziva as zva

ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ZivaSaveandLoadWeights.ui')
list_form, list_base = load_ui_type(ui_file)

class ZivaSaveLoadUI(list_form, list_base):
	def __init__(self, parent = get_maya_window()):
		self.window_name = 'ZivaSaveLoadScript'
		if window(self.window_name, exists = True):
			deleteUI(self.window_name)
			
		super(ZivaSaveLoadUI, self).__init__(parent)
		self.selectedObject = ""
		self.setupUi(self)
		self.SavePathButton.clicked.connect(self.SaveLocationCommand)
		self.SaveApplyButton.clicked.connect(self.SaveLocationApplyCommand)
		self.LoadPathFileSelection.clicked.connect(self.ImportLocationLoadCommand)
		self.LoadApplyButton.clicked.connect(self.ImportApplyCommand)
		
		self.ZivaRenameNodes.clicked.connect(self.ZivaRenameNodesClick)
		
		self.MirrorRtoL.clicked.connect(self.MirrorRtoLClick)
		self.MirrorLtoR.clicked.connect(self.MirrorLtoRClick)
		
		self.AllSaveCB.clicked.connect(self.TurnSelectionOff)
		self.SelectionSaveCB.clicked.connect(self.TurnAllOff)
		
	def SaveLocationApplyCommand(self):
		z = zva.Ziva()
		if self.AllSaveCB.isChecked() == True:
			z.retrieve_from_scene()
		else:
			z.retrieve_from_scene_selection(solver= self.SolverSaveCB.isChecked(),bones= self.BonesSaveCB.isChecked(),tissues= self.TissueSaveCB.isChecked(),attachments= self.AttachSaveCB.isChecked(),materials= self.MaterialsSaveCB.isChecked(),
			fibers= self.FibersSaveCB.isChecked(),embedder= self.EmbedderSaveCB.isChecked())
			z.retrieve_from_scene_selection(connections=False)
		RebuiltPath = (self.SaveFilePath.text() + "/" + self.SaveFileName.text())
		RebuiltPath = RebuiltPath.replace('/', '\\\\')
		print RebuiltPath
		z.write(RebuiltPath)
	
	def SaveLocationCommand(self):
		fileOpenDialogPath = mc.fileDialog( m = 1,dm='*.ziva' )
		if fileOpenDialogPath != "":
			filePath = fileOpenDialogPath.rsplit('/', 1)
			self.SaveFileName.clear()
			self.SaveFilePath.clear()
			self.SaveFileName.insert(filePath[1])
			self.SaveFilePath.insert(filePath[0])
	def TurnSelectionOff(self):
		if self.AllSaveCB.isChecked() == True:
			self.SelectionSaveCB.setChecked(False)
	
	def TurnAllOff(self):
		if self.SelectionSaveCB.isChecked() == True:
			self.AllSaveCB.setChecked(False)
	
	def ImportLocationLoadCommand(self):
		fileImportDialogPath = mc.fileDialog( m = 0,dm='*.ziva' )
		self.LoadFilePath.insert(fileImportDialogPath)
	
	def ImportApplyCommand(self):
		z = zva.Ziva()
		z.retrieve_from_file('C:\\Temp\\tempfull.ziva')
		RebuiltImportPath = self.LoadFilePath.text()
		RebuiltImportPath = RebuiltImportPath.replace('/', '\\\\')
		z.retrieve_from_file(RebuiltImportPath)
		z.build(solver=self.SolverImportCB.isChecked(), bones=self.BoneImportCB.isChecked(), tissues=self.TissuesImportCB.isChecked(), attachments=self.AttachImportCB.isChecked(), materials=self.MaterialsImportCB.isChecked(), fibers=self.FibersImportCB.isChecked(), embedder=self.EmbedderImportCB.isChecked(), cloth=self.ClothImportCB.isChecked(), lineOfActions=self.LActionLoadCB.isChecked())
	
	def ZivaRenameNodesClick(self):
		zMaya.rename_ziva_nodes()	
		
	def MirrorRtoLClick(self):
		z = zva.Ziva()
		z.retrieve_from_scene_selection()
		z.string_replace('^r_','l_')
		z.build()

	def MirrorLtoRClick(self):
		z = zva.Ziva()
		z.retrieve_from_scene_selection()
		z.string_replace('^l_','r_')
		z.build()

		
def run_plugin():
	ex = ZivaSaveLoadUI()
	ex.show()
