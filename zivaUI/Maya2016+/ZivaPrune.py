from pymel.core import *
import os
from loadUiFile import get_maya_window, load_ui_type
import loadUiFile
import maya.mel as mm
import maya.cmds as mc
import zBuilder.zMaya as zMaya
import zBuilder.setup.Ziva as zva

ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ZivaPruneWeights.ui')
list_form, list_base = load_ui_type(ui_file)

class ZivaPruneUI(list_form, list_base):
	def __init__(self, parent = get_maya_window()):
		self.window_name = 'ZivaPruneScript'
		if window(self.window_name, exists = True):
			deleteUI(self.window_name)
			
		super(ZivaPruneUI, self).__init__(parent)
		self.selectedObject = ""
		self.setupUi(self)
		self.generateAttachmentList()
		self.Prune_QButton.clicked.connect(self.pruneCommand)
		self.RoundUp_QButton.clicked.connect(self.RoundUpCommand)
		self.RefereshQButton.clicked.connect(self.refreshAttachmentConnections)
		
	def generateAttachmentList(self):
		self.selectedObject = mc.ls(sl= True)[0]
		Attachmentlist = zMaya.get_zAttachments(self.selectedObject)
		AttachmentMaterialslist = zMaya.get_zMaterials(self.selectedObject)
		if AttachmentMaterialslist!= []:
			AttachmentMaterialslist.pop(0)
		AttachmentTetslist = zMaya.get_zTets(self.selectedObject)
		AttachmentZFiberslist = zMaya.get_zFibers(self.selectedObject)
		self.selectedObject = self.selectedObject.rsplit('_',1)[0]	
		Attachmentfilter = []
		for attachnode in Attachmentlist:
			if attachnode.startswith(self.selectedObject) == True:
				Attachmentfilter.append(attachnode)
		self.ZAttachmentQComboBox.clear()
		for ZAttachmentNode in Attachmentfilter:
			if mc.objExists(ZAttachmentNode):
				self.ZAttachmentQComboBox.addItem(ZAttachmentNode)
		for ZMaterials in AttachmentMaterialslist:
			self.ZAttachmentQComboBox.addItem(ZMaterials)
		for ZTets in AttachmentTetslist:
			self.ZAttachmentQComboBox.addItem(ZTets)			
		for ZFibers in AttachmentZFiberslist:
			self.ZAttachmentQComboBox.addItem(ZFibers)		
	
	def refreshAttachmentConnections(self):
		self.ZAttachmentQComboBox.clear()
		self.generateAttachmentList()
	
	def pruneCommand(self):
		pruneValue = float(self.floatValueQline.text())
		pruneObject = str(self.ZAttachmentQComboBox.currentText())
		print pruneValue
		print pruneObject
		if pruneValue < 1:
			self.pruneCycle(pruneValue, pruneObject)
		else:
			print "please implement a value lower then 1"
	def pruneCycle(self, pruneValue, pruneObject):
		#baseWeightList = mc.getAttr(pruneObject + ".weightList[0].weights")[0]
		baseVertexCounter = cmds.polyEvaluate( v=True )
		#print len(baseWeightList)
		print baseVertexCounter
		pruneNumerator = 0
		while(pruneNumerator < baseVertexCounter):
		#while(pruneNumerator < len(baseWeightList)):
			pruneObjectValue = getAttr(pruneObject + ".weightList[0].weights[" + str(pruneNumerator) + "]")
			if pruneValue>pruneObjectValue:
				mc.setAttr(pruneObject + ".weightList[0].weights[" + str(pruneNumerator) + "]", 0)
			pruneNumerator += 1
			
			
	def RoundUpCommand(self):
		RoundUpValueCmd = float(self.roundUpfloatValueQline.text())
		RoundUpObjectCmd = str(self.ZAttachmentQComboBox.currentText())
		if RoundUpValueCmd < 1:
			self.RoundUpCycle(RoundUpValueCmd, RoundUpObjectCmd)
		else:
			print "please implement a value lower then 1"
			
	def RoundUpCycle(self, RoundUpValue, RoundUpObject):
		baseWeightList = mc.getAttr(RoundUpObject + ".weightList[0].weights")[0]
		RoundUpNumerator = 0
		while(RoundUpNumerator < len(baseWeightList)):
			RoundUpObjectValue = getAttr(RoundUpObject + ".weightList[0].weights[" + str(RoundUpNumerator) + "]")
			if RoundUpValue<RoundUpObjectValue:
				mc.setAttr(RoundUpObject + ".weightList[0].weights[" + str(RoundUpNumerator) + "]", 1.0)
			RoundUpNumerator += 1
			
	
def run_plugin():
	ex = ZivaPruneUI()
	ex.show()
